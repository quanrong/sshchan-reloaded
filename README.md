# SSHCHAN RELOADED
Textboard server accesed through SSH.
Thanks to Chibi and all the original contributors!

The text below is based on the original README.md, which is out of date. I will be updating it as I work with the code.
PRs, cleanup & new features all welcome; I'll be working on improving the GUI/TUI, adding moderator tools, documentation and more.

## sshchan reloaded

A textboard environment implemented in Python. At the moment it is a script meant to be run server-side by a user connecting to that server using ssh. It hopes to be configurable and secure.

How to Install / Dependencies
---

**Please read [`docs/sshchan-deployment.txt`](docs/sshchan-deployment.txt) for a full guide on setting up a functional, safe chan.**

	git clone https://gitlab.com/quanrong/sshchan-reloaded.git sshchan-reloaded
	cd sshchan-reloaded
	python3 setup.py

That should set up the basic chan for you. From there, read the documentation [`docs/config.md`](docs/config.md) for more admin stuff.

### gui status

GUI will require `urwid` module, which is a third-party module. It can be obtained from https://pypi.python.org/pypi/urwid or installed from your distro's package manager.
There is also included an alternative, legacy display which doesn't rely on the `urwid` module. It can be enabled through the `admin.py` script.

Configuration
---

The `admin.py` script helps you to configure the server. Run that and type `h` at the command prompt to get some help.

Roadmap
---

* Working urwid GUI
* Wordfilters
* Reports, banning and warnings
* File uploads via one of the pomf clones with shortened links added to post contents
* Secure admin authentication (via GPG)
* User accounts

Related projects
---
* einchan's repo from which this one is forked: [sshchan](https://github.com/einchan/sshchan)
* undoall's [sshchan-functional](https://github.com/undo-all/sshchan-functional)
* blha303's [sshchan-web](https://github.com/blha303/sshchan-web)
